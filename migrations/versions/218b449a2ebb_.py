"""empty message

Revision ID: 218b449a2ebb
Revises: 
Create Date: 2018-12-24 13:46:08.667509

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '218b449a2ebb'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('diary',
    sa.Column('entryid', sa.Integer(), nullable=False),
    sa.Column('userid', sa.Integer(), nullable=True),
    sa.Column('categoryid', sa.String(length=64), nullable=True),
    sa.Column('description', sa.String(length=500), nullable=True),
    sa.Column('price', sa.Float(precision=2), nullable=True),
    sa.Column('feels', sa.Float(precision=2), nullable=True),
    sa.Column('timestamp', sa.DateTime(timezone=6), nullable=True),
    sa.PrimaryKeyConstraint('entryid')
    )
    op.create_table('foodcategories',
    sa.Column('id', sa.Integer(), nullable=True),
    sa.Column('categoryid', sa.String(length=64), nullable=False),
    sa.Column('categoryname', sa.String(length=64), nullable=True),
    sa.Column('categoryshortname', sa.String(length=64), nullable=True),
    sa.Column('description', sa.String(length=140), nullable=True),
    sa.Column('multiplier', sa.Float(), nullable=True),
    sa.PrimaryKeyConstraint('categoryid')
    )
    op.create_table('users',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('firstname', sa.String(length=64), nullable=True),
    sa.Column('lastname', sa.String(length=64), nullable=True),
    sa.Column('username', sa.String(length=64), nullable=False),
    sa.Column('password', sa.String(length=128), nullable=False),
    sa.Column('email', sa.String(length=128), nullable=True),
    sa.Column('lastActive', sa.DateTime(timezone=6), nullable=True),
    sa.Column('registrationDate', sa.DateTime(timezone=6), nullable=True),
    sa.Column('daysInactive', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('username')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('users')
    op.drop_table('foodcategories')
    op.drop_table('diary')
    # ### end Alembic commands ###
