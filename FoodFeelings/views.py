from FoodFeelings import app
from FoodFeelings.forms import LoginForm, RegistrationForm
from flask import session, render_template, request, redirect, url_for


@app.route("/")
def Index():
    if not session.get("logged_in"):
        form = LoginForm(request.form)
        return render_template("auth/login.html", title="Food Feelings", form=form)
    return redirect(url_for("MyFeelings"))
