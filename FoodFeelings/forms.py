import datetime
from flask import flash
from wtforms import Form, ValidationError
from wtforms.fields import (
    StringField, IntegerField, PasswordField, FloatField,
    TextAreaField, SelectField, BooleanField
    )
from wtforms.validators import (
    DataRequired, InputRequired, length, Email,
    Regexp, EqualTo
)
from us import STATES
from iso3166 import countries
from werkzeug.security import check_password_hash


class LoginForm(Form):
    username = StringField(validators=[InputRequired()])
    password = PasswordField(validators=[])

class RegistrationForm(Form):
    firstname = StringField(label="First Name")
    lastname = StringField(label="Last Name")
    username = StringField(label="Username")
    email = StringField('Email Address',
        validators=[InputRequired(), Email(), Regexp('[^@]+@[^@]+\.[^@]+')]) # Use for email verification
    password = PasswordField('New Password', [
        InputRequired(), length(min=8, max=20),
        EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField(validators=[DataRequired()])
