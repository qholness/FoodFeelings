INSERT IGNORE INTO foodcategories (id, categoryid, categoryname, categoryshortname, description, multiplier)
VALUES 
    (1, 'FF', 'Fast Food', 'Fast Food', 'That greasy good stuff', 0.5),
    (2, 'FC', 'Fast Casual', 'Fast Casual', 'A step up from fast food', .7),
    (3, 'Frutas', 'Fruit', 'Fruit', 'Fruit of the Earth', 1.5),
    (4, 'Vegeta', 'Vegetables', 'Veggies', 'Vegetables of the Earth', 1.5),
    (5, 'Misc', 'Other', 'Misc', 'Something a bit... odd.', 1);