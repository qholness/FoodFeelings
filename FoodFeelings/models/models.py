from FoodFeelings import db
from sqlalchemy import (
    Integer, String, ForeignKey
    , Column, DateTime
    , Boolean, Float
)
from werkzeug.security import check_password_hash # Generate in view, check in models.

class User(db.Model):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    firstname = Column(String(64))
    lastname = Column(String(64))
    username = Column(String(64), nullable=False, unique=True)
    password = Column(String(128), unique=False, nullable=False)
    email = Column(String(128))
    lastActive = Column(DateTime(6))
    registrationDate = Column(DateTime(6))
    daysInactive = Column(Integer)

    def verify_password(self, password):
        return check_password_hash(self.password, password)

class FoodCategory(db.Model):
    __tablename__ = "foodcategories"
    id = Column(Integer)
    categoryid = Column(String(64), primary_key=True)
    categoryname = Column(String(64))
    categoryshortname = Column(String(64))
    description = Column(String(140))
    multiplier = Column(Float)

class Diary(db.Model):
    __tablename__ = "diary"
    entryid = Column(Integer, primary_key=True)
    userid = Column(Integer)
    categoryid = Column(String(64))
    description = Column(String(500))
    price = Column(Float(2))
    feels = Column(Float(2))
    timestamp = Column(DateTime(6))
