from flask import session, flash, redirect
from functools import wraps


def logged_in(func):
    @wraps(func)
    def d(*args, **kwargs):
        if session.get("logged_in"):
            return func(*args, **kwargs)
        flash("You must be logged in to view this page", category="info")
        return redirect(utils.redirect_url())
    return d


def not_logged_in(func):
    @wraps(func)
    def d(*args, **kwargs):
        if session.get("logged_in"):
            flash("Invalid request", category="info")
            return redirect(utils.redirect_url())
        return func(*args, **kwargs)
    return d
