from FoodFeelings import app, db
from FoodFeelings.models.models import Diary, FoodCategory
from FoodFeelings.feelings.forms import SimpleFoodForm
from FoodFeelings.wrappers import logged_in
from flask import render_template, session, request, redirect, flash, url_for
from FoodFeelings.utilities import Utilities
from FoodFeelings.feelings.utilities import utilities
import pandas as pd

utils = Utilities()
feels_utils = utilities()

# User activities and service API
@app.route("/myFeelings")
@logged_in
def MyFeelings():
    title = "{}'s Food Feelings".format(session["username"])
    categories = [(i.categoryid, i.categoryname) for i in FoodCategory.query.all()]
    simplefoodform = SimpleFoodForm(request.form)
    diary = GetDiary()
    return render_template("feelings/myfeelings.html", title=title, simplefoodform=simplefoodform
    , diary=diary)

@app.route("/share-my-simple-feelings", methods=["POST"])
@logged_in
def ShareSimpleFeelings():
    form = SimpleFoodForm(request.form)
    desc, valid = form.validate()
    if valid:
        newEntry = Diary()
        newEntry.userid = session["userid"]
        newEntry.categoryid = form.foodcategory.data.categoryid
        newEntry.description = form.description.data
        newEntry.price = form.price.data
        newEntry.feels = form.feels.data
        newEntry.timestamp = utils.get_timestamp()
        db.session.add(newEntry)
        db.session.commit()
        return desc
    return desc

@app.route('/calc-feels')
@logged_in
def CalcFeels():
    df = feels_utils.diary_query()
    df = pd.read_sql(df.statement, db.session.bind, coerce_float=False)
    if df.empty: return "Diary is empty"
    feels_array = [None] * len(df)
    avg = df['price'].median()
    prices = df['price'] * df['Multiplier']
    feels = df['feels']
    pf = zip(prices, feels)
    rolling_score = 0
    for idx, (p,f) in enumerate(pf):
        penalty = p/avg
        feels = feels_utils.convert_feels(f)
        feels = int(feels * penalty if feels < feels_utils.bad_feels_threshhold else feels // penalty)
        rolling_score += feels
        rolling_score = feels_utils.check_score(rolling_score)
        rolling_score = feels_utils.diminishing_returns(rolling_score)
        feels_array[idx] = int(rolling_score)
    df['rolling_score'] = feels_array
    df["abs_feels"] = abs(df["feels"])
    grouped_by_category = df[["Category", "abs_feels"]]\
        .groupby("Category")\
        .sum()\
        .reset_index()
    print(grouped_by_category)
    return df.to_json()

@app.route('/get-diary')
@logged_in
def GetDiary():
    return render_diary(feels_utils.diary_query().all())

def render_diary(d):
    if not d: return "Diary is empty"
    d = sorted(d, key=lambda x: x.Diary.timestamp)
    score = 0
    avg = sum(_.Diary.price for _ in d)/len(d)
    header = """<table class=table>
        <thead>
            <td>Category</td>
            <td>Description</td>
            <td>Spent</td>
            <td>Feels</td>
            <td>Feel Score &#8482</td>
            <td>Date</td>
        </thead>"""
    footer = "</table>"

    for idx, _ in enumerate(d):
        penalty = _.Diary.price * _.Multiplier /avg
        feels = feels_utils.convert_feels(_.Diary.feels)
        feels = int(feels * penalty if feels < feels_utils.bad_feels_threshhold else feels)
        score += feels
        score = feels_utils.check_score(score)
        score = int(feels_utils.diminishing_returns(score))
        date = "/".join(map(str, (_.Diary.timestamp.year, _.Diary.timestamp.month, _.Diary.timestamp.day)))
        minute = "0{}".format(_.Diary.timestamp.minute) if len(str(_.Diary.timestamp.minute)) == 1 else _.Diary.timestamp.minute
        time = ":".join(map(str, (_.Diary.timestamp.hour, minute)))
        header += """<tr>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
            <td>{4}</td>
            <td>{5}</td>
        </tr>""".format(_.Category, _.Diary.description, _.Diary.price, feels, score, " ".join((date, time)))

    return header + footer
