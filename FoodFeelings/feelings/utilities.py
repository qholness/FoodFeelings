from FoodFeelings.models.models import Diary, FoodCategory
from flask import session
from math import log


class utilities:
    bad_feels_threshhold = 25
    good_feels_thresshold = 75
    max_score = 1000
    min_score = -1000

    def diary_query(self):
        return Diary\
        .query\
        .filter_by(userid = session["userid"])\
        .join(FoodCategory, FoodCategory.categoryid==Diary.categoryid)\
        .add_column(FoodCategory.multiplier.label("Multiplier"))\
        .add_column(FoodCategory.categoryname.label("Category"))\
        .order_by(Diary.timestamp.asc())

    def average(self, x, attr):
        return sum(getattr(_, attr) for _ in x)/len(x)


    def convert_feels(self, x):
        return int(x - self.bad_feels_threshhold if x < self.bad_feels_threshhold
            else x if x > self.good_feels_thresshold else x
        )

    def check_score(self, score):
        return (self.max_score if score > self.max_score
            else self.min_score if  score < self.min_score
            else score)
        
    def diminishing_returns(self, score):
        if score:
            return score - log(score**2)
        return score