import datetime
from flask import flash
from wtforms import Form, ValidationError
from wtforms.fields import (
    StringField, IntegerField, PasswordField, FloatField,
    TextAreaField, SelectField, BooleanField
    )
from wtforms.fields.html5 import DecimalRangeField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import (
    DataRequired, InputRequired, length, Email,
    Regexp, EqualTo
)
from us import STATES
from iso3166 import countries
from werkzeug.security import check_password_hash
from FoodFeelings.models.models import FoodCategory
from FoodFeelings.utilities import Utilities
utils = Utilities()

def category_factory():
    """Food category factory"""
    return sorted(FoodCategory.query.all(), key=lambda x: x.id)

class SimpleFoodForm(Form):
    """Form for submitting a simple meal entry"""
    foodcategory = QuerySelectField("Food Category", query_factory=category_factory
        ,get_label="categoryshortname", allow_blank=False)
    description = TextAreaField("Description of the food", validators=[length(15)])
    price = FloatField("Cost")
    feels = DecimalRangeField("How you felt")

    def validate(self):
        if self.foodcategory.data == "":
            return ("Invalid food category", False)
        if self.description.data == "":
            return ("Empty description", False)
        return ("Feelings shared. Thanks bro.", True)
