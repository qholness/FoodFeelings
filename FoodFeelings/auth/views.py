from FoodFeelings import db
from FoodFeelings.utilities import Utilities
from FoodFeelings.models.models import (
    User
)
from FoodFeelings.forms import RegistrationForm, LoginForm
from FoodFeelings.wrappers import logged_in, not_logged_in
from flask import session, request, redirect, url_for, flash, render_template
from werkzeug.security import check_password_hash, generate_password_hash
from . import auth

utils = Utilities()

@auth.route("/register")
@not_logged_in
def RegisterPage():
    form = RegistrationForm(request.form)
    return render_template("auth/register.html", title="Sign up", form=form)


# Call service
@auth.route("/register-user", methods=["POST"])
def Register():
    form = RegistrationForm(request.form)
    firstname, lastname = form.firstname.data, form.lastname.data
    username, email = form.username.data, form.email.data
    password = form.password.data

    if utils.CheckUsername(username):
        flash("This username is in use.", category="warning")
        return redirect(url_for("Index"))

    if utils.CheckEmail(email):
        flash("This email is in use.", category="warning")
        return redirect(url_for("Index"))

    newUser = User()
    newUser.firstname, newUser.lastname = firstname, lastname
    newUser.username, newUser.email = username, email
    newUser.password = generate_password_hash(password)

    database.session.add(newUser)
    database.session.commit()
    flash("You have successfully registered.")
    return redirect(url_for("Index"))

def login(u=None, password=None):
    if u:
        if check_password_hash(u.password, password):
            utils.login_user(u)
            return redirect(url_for("MyFeelings"))
        else:
            flash("Invalid password", category="warning")
            return redirect(utils.redirect_url())
    else:
        flash("Invalid username", category="warning")
        return redirect(utils.redirect_url())

@auth.route("/login", methods=["POST"])
def Login():
    form = LoginForm(request.form)
    username = form.username.data
    password = form.password.data
    u = User\
        .query\
        .filter(User.username == username)\
        .first()
    return login(u, password)


@logged_in
@auth.route("/logout")
def Logout():
    session.clear()
    return redirect(url_for("Index"))
