from flask import request, url_for, session
from FoodFeelings.models.models import User, FoodCategory
import datetime

class Utilities:
    def get_timestamp(self):
    	return datetime.datetime.now()

    def redirect_url(self, default='Index'):
        return request.args.get('next') or \
            request.referrer or \
            url_for(default)

    def login_user(self, USER):
        session["logged_in"] = 1
        session["userid"] = USER.id
        session["username"] = USER.username

    def CheckUsername(self, username):
        if User.query.filter(User.username == username).first():
            return True
        return False

    def CheckEmail(self, email):
        if User.query.filter(User.email == email).first():
            return True
        return False

    def FoodCategories(self):
        return FoodCategory.query.all()
