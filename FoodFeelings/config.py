class Config(object):
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    NO_BACKSLASH_ESCAPES = True
    SECRET_KEY='SomeProductionKey'
    SQLALCHEMY_DATABASE_URI = 'sqlite:////media/serverbox/Databases/food.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class Development(Config):
    DEBUG = True
    ## Development/Test Database ##
    # SQLALCHEMY_DATABASE_URI = "postgresql://foodie:birdstheword@localhost/diet"


class Testing(Development):
    DEBUG = True
    # SQLALCHEMY_DATABASE_URI = "postgresql://foodie:birdstheword@localhost/diet"
    TESTING = True
    FIRSTNAME='test'
    LASTNAME='user'
    USERNAME='test_user'
    PASSWORD='test_password'
    EMAIL='test@test.com'

class Production(Config):
    DEBUG=False
    SQLALCHEMY_DATABASE_URI = 'sqlite:////media/serverbox/Databases/food_prod.db'
