from wtforms import Form, ValidationError
from wtforms.fields import (
    StringField, IntegerField, PasswordField, FloatField,
    TextAreaField, SelectField, BooleanField
    )
from wtforms.validators import (
    DataRequired, InputRequired, length, Email,
    Regexp, EqualTo
)

class ChangePassword(Form):
    oldPassword = PasswordField(validators=[InputRequired()])
    password = PasswordField(validators=[InputRequired()])
    passwordConf = PasswordField(validators=[InputRequired()])