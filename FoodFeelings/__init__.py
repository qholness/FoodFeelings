from logging import INFO
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from FoodFeelings.db import db
from FoodFeelings.auth import auth
import psycopg2
import uuid


app = Flask(__name__)
app.config.from_object('FoodFeelings.config.Development')
db.init_app(app)
migrate = Migrate(app, db)
app.register_blueprint(auth)



from FoodFeelings import models
from FoodFeelings import views
from FoodFeelings.feelings import views
