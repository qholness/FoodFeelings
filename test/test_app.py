import os
import tempfile

import pytest

from flask import Flask, request
from FoodFeelings import app
from FoodFeelings import database
from FoodFeelings.models.models import User
from werkzeug.security import generate_password_hash


app.config['TESTING'] = True
app.config['FIRSTNAME'] = 'test'
app.config['LASTNAME'] = 'user'
app.config['USERNAME'] = 'test_user'
app.config['PASSWORD'] = 'test_password'
app.config['EMAIL'] = 'test@test.com'


def create_app(self):
    app.config.from_object('FoodFeelings.config.Testing')
    app.config['LIVESERVER_PORT'] = 5000
    app.config['LIVESERVER_TIMEOUT'] = 10
    return app

@pytest.fixture
def client():
    app = Flask(__name__)
    app.config.from_object('FoodFeelings.config.Testing')
    database.session.execute(f"TRUNCATE {User.__tablename__} RESTART "
        "IDENTITY CASCADE;")
    client = app.test_client()
    yield client

def test_00_add_test_user(client):
    test_user = User(
        firstname=app.config['FIRSTNAME'],
        lastname=app.config['LASTNAME'],
        username=app.config['USERNAME'],
        password=generate_password_hash(app.config['PASSWORD']),
        email=app.config['EMAIL']
    )
    database.session.add(test_user)
    database.session.commit()

def test_empty_db(client):
    re = client.get('/')
    assert re is not None, "rv is None"

def login(client, username, password):
    return client.post('/login', data=dict(
        username=username,
        password=password
    ), follow_redirects=True)

def test_login(client):
    rv = login(client, app.config['USERNAME'], app.config['PASSWORD'])
    print(dir(rv))
    print(rv.data)
#     # assert 'You were logged in'
#     print("Something here")