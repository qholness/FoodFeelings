from FoodFeelings import app, database, migrate
from FoodFeelings.models.models import User
from werkzeug.security import generate_password_hash

app.config.from_object('FoodFeelings.config.Testing')
database.session.execute(f"TRUNCATE {User.__tablename__} RESTART IDENTITY CASCADE;")
test_user = User(
    firstname=app.config['FIRSTNAME'],
    lastname=app.config['LASTNAME'],
    username=app.config['USERNAME'],
    password=generate_password_hash(app.config['PASSWORD']),
    email=app.config['EMAIL']
)
database.session.add(test_user)
database.session.commit()

if __name__ == '__main__':
    app.run()