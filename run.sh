#!/usr/bin/bash


source ../.virtualenvs/FoodFeelings/bin/activate
export FLASK_APP=main.py
flask run --host $1 --port $2
